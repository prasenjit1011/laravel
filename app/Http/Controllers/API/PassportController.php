<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\HasApiTokens;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class PassportController extends Controller
{
    public $successStatus = 200;
	public function login(){
		//$arr = array('email'=>'admin@gmail.com','password'=>'12345678');
		$arr = array('email'=>request('email'),'password'=>request('password'));
		if(Auth::attempt($arr,true)){
			$user = Auth::user();
			$success['token'] = $user->createToken('MyApp')->accessToken;
			return response()->json(['success'=>$success],$this->successStatus);
		}else{
			return response()->json(['error'=>'Unauthorized'],401);
		}
	}
	public function getUserDetails(){
		$user = Auth::user();
		return response()->json(['userid'=>$user->id,'userdata'=>$user],$this->successStatus);
	}
}
