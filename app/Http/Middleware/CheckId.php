<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		//dd(1254);
		if(!isset(auth::user()->id)){
			return redirect('/');
		}
		elseif(auth::user()->id > 1){
			return redirect('home');
		}
        return $next($request);
    }
}
